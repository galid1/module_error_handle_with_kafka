package com.galid.module_error_handle_with_kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

public class SpringMain implements ApplicationRunner {
    @Autowired
    private Producers producers;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        producers.sendMessage("mytopic", "hihihi!!!");
    }
}
