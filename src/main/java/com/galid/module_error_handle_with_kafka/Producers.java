package com.galid.module_error_handle_with_kafka;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@RequiredArgsConstructor
public class Producers {
    private final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);
    private final KafkaTemplate<String, String> kafkaTemplate;
    private int repeatCount = 0;


    public void sendMessage(String topic, String payload) {
        logger.info("sending payload = '{}' to topice='{}'", payload, topic);
        ListenableFuture<SendResult<String, String>> listenable = kafkaTemplate.send(topic, payload);

        listenable.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                repeatCount++;
                System.out.println("repeat : " + repeatCount);
                System.out.println(ex);
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                System.out.println("what ??");
                System.out.println(result.toString());
            }
        });
    }
}
