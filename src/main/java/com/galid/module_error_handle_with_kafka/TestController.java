package com.galid.module_error_handle_with_kafka;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.function.Consumer;

@RestController
@RequiredArgsConstructor
public class TestController {
    private final Producers producers;

    @GetMapping("/")
    public void test() {
        producers.sendMessage("baeldung", "HIHIHII");
    }
}
