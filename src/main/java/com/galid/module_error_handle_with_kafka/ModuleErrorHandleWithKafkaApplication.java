package com.galid.module_error_handle_with_kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModuleErrorHandleWithKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModuleErrorHandleWithKafkaApplication.class, args);
    }

}
