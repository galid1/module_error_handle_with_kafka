package com.galid.consumer;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class TestController {
    private final Consumers consumers;

    @GetMapping("/")
    public String test() throws IOException {
        consumers.consume();
        return "OK";
    }
}
