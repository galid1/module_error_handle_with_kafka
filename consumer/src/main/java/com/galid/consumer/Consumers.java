package com.galid.consumer;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class Consumers {
    private final int WRITE_BATCH_SIZE = 10;

//    @KafkaListener(topics = "baeldung")
    public void listenGroupFoo(String message) {
        System.out.println("received message foo : " + message);
    }

    public void consume() throws IOException {
        KafkaConsumer<String, String> consumer = getStringStringKafkaConsumer();
        consumer.subscribe(Arrays.asList("baeldung"));

        ArrayList outputBuffer = new ArrayList<>();

        File outFile = new File("C://test//testOut.txt");
        outFile.getParentFile().mkdirs();
        outFile.createNewFile();

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));
            System.out.println("poll count is : " + records.count());

            if(records.count() > 0) {
                for (ConsumerRecord<String, String> record: records) {
                    outputBuffer.add(makeARecord(record));
                    System.out.println("added to output buffer , size is : " + outputBuffer.size());
                    for (Object a : outputBuffer) {
                        System.out.println((String)a);
                    }

                    if (outputBuffer.size() >= WRITE_BATCH_SIZE) {
                        // 파일에 다썻으면 다 읽었다는 증거이므로, 이제 커밋
                        writeBufferToFile(outFile, outputBuffer);
                        consumer.commitSync();
                        outputBuffer.clear();
                    }
                    else {
                        outputBuffer.add(makeARecord(record));
                    }
                }
            }
        }
    }

    private KafkaConsumer<String, String> getStringStringKafkaConsumer() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "myGroup");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        return consumer;
    }

    private void writeBufferToFile(File outFile, ArrayList<String> outputBuffer) throws IOException {
        FileWriter writer = new FileWriter(outFile);

        if (outFile.canWrite()) {
            outputBuffer.forEach(m -> {
                try {
                    writer.append(m);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            writer.flush();
            writer.close();
        }
    }

    private String makeARecord(ConsumerRecord<String, String> record) {
        return "Partiion: " + record.partition() + " , offset: " + record.offset() + " , key: " + record.key()
                + " , Value : " + record.value() + "\n";
    }
}
